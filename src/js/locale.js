import en from "../locales/en.json";
import pt from "../locales/pt.json";

const extract = ({ lang }) => {
    const locales = {
        "en-us": en,
        "pt-br": pt,
    };

    return locales[lang];
};

const messages = extract({
    lang: document.documentElement.lang.toLowerCase(),
});

export default messages;
