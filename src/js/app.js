import Alpine from "alpinejs";
window.Alpine = Alpine;

import _ from "lodash";

import messages from "./locale";
import "./util";

Alpine.data("content", () => ({
    t: (key, ...params) => {
        const value = _.get(messages, key);
        return value ? value.format(params) : key;
    },
    data: (key) => _.get(messages, key),
}));

Alpine.start();
